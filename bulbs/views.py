from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from yeelight import discover_bulbs

from .utils import set_rgb_color, set_flow
from .serializers import ListBulbsSerializer, SetRGBColorSerializer, SetFlowSerializer


class BulbsListView(APIView):
    def get(self, request, *args, **kwargs):
        bulbs = discover_bulbs()
        serializer = ListBulbsSerializer(bulbs, many=True)

        return Response(serializer.data)

    def post(self, request):
        serializer = SetRGBColorSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        ip = serializer.data.get('ip')
        red, green, blue = serializer.data.get('color')
        brightness = serializer.data.get('brightness')

        try:
            set_rgb_color(ip, red, green, blue, brightness)
        except Exception:
            raise ValueError('Error setting color to the bulb')

        return Response(serializer.data, status=status.HTTP_200_OK)


class SetBulbFlowView(APIView):
    def post(self, request):
        serializer = SetFlowSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.data

        try:
            set_flow(data.get('ip'), data.get('transitions'), data.get('count'))
        except Exception:
            raise ValueError('Error setting flow to the bulb')

        return Response(serializer.data, status=status.HTTP_200_OK)


bulbs_list_view = BulbsListView.as_view()
set_bulb_flow_view = SetBulbFlowView.as_view()
