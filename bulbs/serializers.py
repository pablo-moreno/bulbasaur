from rest_framework import serializers


class CapabilitiesSerializer(serializers.Serializer):
    id = serializers.CharField(max_length=64)
    power = serializers.CharField(max_length=3)
    bright = serializers.CharField(max_length=3)
    color_mode = serializers.CharField(max_length=9)
    rgb = serializers.SerializerMethodField(read_only=True)

    def get_rgb(self, data):
        rgb_hex = '{:02x}'.format(int(data.get('rgb'))).upper()
        color = [
            int(rgb_hex[i:i + 2], 16)
            for i in range(0, len(rgb_hex), 2)
        ]
        return color


class ListBulbsSerializer(serializers.Serializer):
    ip = serializers.IPAddressField()
    port = serializers.IntegerField(max_value=64000)
    capabilities = CapabilitiesSerializer()


class ColorSerializer(serializers.Serializer):
    red = serializers.IntegerField(max_value=255, min_value=0, default=0)
    green = serializers.IntegerField(max_value=255, min_value=0, default=0)
    blue = serializers.IntegerField(max_value=255, min_value=0, default=0)


class SetRGBColorSerializer(serializers.Serializer):
    ip = serializers.IPAddressField()
    color = serializers.ListField(child=serializers.IntegerField(min_value=0, max_value=255))
    brightness = serializers.IntegerField(min_value=0, max_value=100, default=50)


class TransitionColorSerializer(serializers.Serializer):
    color = serializers.ListField(child=serializers.IntegerField(min_value=0, max_value=255))
    duration = serializers.FloatField(min_value=0, max_value=30.0)


class SetFlowSerializer(serializers.Serializer):
    ip = serializers.IPAddressField()
    repeat = serializers.IntegerField(min_value=0)
    transitions = TransitionColorSerializer(many=True)

    def validate_transitions(self, transitions):
        if len(transitions) > 9:
            raise ValueError('Cannot add more than 9 transitions')

        return transitions
