from django.urls import path
from .views import bulbs_list_view, set_bulb_flow_view

app_name = 'bulbs'


urlpatterns = [
    path('', bulbs_list_view, name='bulbs_list_view'),
    path('flows', set_bulb_flow_view, name='set_bulb_flow_view'),
]
