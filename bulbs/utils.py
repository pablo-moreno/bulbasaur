from yeelight import Bulb, Flow, RGBTransition


def set_rgb_color(ip, red, green, blue, brightness=100):
    bulb = Bulb(ip)
    bulb.set_rgb(red, green, blue)
    bulb.set_brightness(brightness)


def set_flow(ip, transitions_dict, repeat):
    bulb = Bulb(ip)

    transitions = []
    for transition_dict in transitions_dict:
        (red, green, blue), duration = transition_dict.get('color'), transition_dict.get('duration') * 1000
        transitions.append(RGBTransition(red, green, blue, duration=duration))

    flow = Flow(transitions=transitions, count=0)
    bulb.start_flow(flow)
