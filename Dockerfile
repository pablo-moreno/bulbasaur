FROM python:3.7.4-slim

WORKDIR /app
COPY . /app

ARG VERSION

ENV DJANGO_SETTINGS_MODULE=config.settings
ENV APP_VERSION=$VERSION

RUN pip install -r requirements.txt && python manage.py collectstatic --noinput && python manage.py migrate

ENTRYPOINT ["gunicorn", "-b", "0.0.0.0:8000", "config.wsgi"]
